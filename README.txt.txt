This module parses through a given word/sentence (input) and returns the number of vowels, consonants and special characters.
It is useful to a very specific audience who need such data to integrate into there website/project.

Example : Input : Hello!
Output : Number of vowels is 2, Number of consonants is 3 and number of special characters is 1.

