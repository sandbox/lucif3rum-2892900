<?php

namespace Drupal\word_breakdown\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'word_breakdown' block.
 *
 * @Block(
 *   id = "word_breakdown_block",
 *   admin_label = @Translation("Word Breakdown"),
 * )
 */
class WordBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\word_breakdown\Form\AddForm');

    return [
      'add_this_page' => $form,
    ];
  }

}
