<?php

namespace Drupal\word_breakdown\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Class AddForm.
 *
 * @package Drupal\word_breakdown\Form\AddForm
 */
class AddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'word_brk_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = [];

    // Any word or sentence Input.
    $form['input'] = [
      '#title' => $this->t('Any word or sentence'),
      '#type' => 'textfield',
      '#weight' => 1,
      '#default_value' => '',
    ];

    // Submit button definition.
    $form['Check'] = [
      '#type' => 'button',
      '#value' => $this->t('Calculate'),
      '#weight' => 2,
      '#ajax' => [
        // Function to call when event on form element triggered.
        'callback' => '::calculatedown',
        'event' => 'click',
        'progress' => [
    // WUT???
          'type' => 'throbber',
          'message' => 'Breaking down the sentence..',
        ],
      ],
    ];

    // Results section markup.
    $form['calculated'] = [
      '#type' => 'markup',
      '#weight' => 3,
      '#prefix' => '<div id="word_breakdown_calculated">',
    // WUTTTT???
      '#suffix' => '</div>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Function to check if leap year or not.
   */
  public function calculatedown(array &$form, FormStateInterface $form_state) {

    $output = '';$num = 0;$num1 = 0;$num2 = 0;$num3 = 0;
    // If input is not empty.
    if (!empty($form_state->getValue('input'))) {

      // Formatting user input.
      $inputz = $form_state->getValue('input');
      $string = strtolower($inputz);
      $vowels = ['a', 'e', 'i', 'o', 'u'];
      $consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
        'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z',
      ];
      $special = ['@', '$', '!', '#'];
      // Find length of the string.
      $len = strlen($string);

      // Loop through each letter.
      for ($i = 0; $i < $len; $i++) {
        if (in_array($string[$i], $vowels)) {
          $num++;
        }
      }
      for ($i = 0; $i < $len; $i++) {
        if (in_array($string[$i], $consonants)) {
          $num2++;
        }
      }
      for ($i = 0; $i < $len; $i++) {
        if (in_array($string[$i], $special)) {
          $num3++;
        }
      }

      // Getting output.
      $output = "Number of vowels is $num, Number of consonants is $num2, Number of special chars is $num3";

    }

    else {
      $output = $this->t('Cannot be empty.');
    }

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#word_breakdown_calculated', $output));
    return $response;
  }

}
